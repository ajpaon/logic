# Changelog

## 2020-06-16

* Allow edits to be cancelled
* Add a refresh button for the document list
* Fix a bug that caused the document list to reverse itself on every render
* Add a "rewind" feature to go backwards in the render pane
