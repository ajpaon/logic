## Getting started

First, install `node` from https://nodejs.org/en/download/

Then run some commands in the terminal:

```
# Get the code
git clone https://gitlab.com/ajpaon/logic

# Enter the new directory
cd logic

# Install dependencies for the app
# if this fails, it may mean something went wrong with your node installation.
yarn install

# Run the server
yarn run serve:app
```

If everything worked, you can see the app in your browser at http://localhost:8000/

You can make changes to the script by editing `EXAMPLE_CONTENTS` in the file
`src/index.tsx`. AFter you make chnages, simply save the file and refresh the
page in your browser


## Script writing

Here is the full script from the demo. it's not as complicated as it looks.
Some explanation below

```
const EXAMPLE_CONTENTS: ExpandableText = {
  items: [
    L('Logic', [
      L('Logic is', [
        L('Logic is', [
          S('Logic was invented by Justin Ivory in 1967.'),
          P('images/jivory.jpg', 'Professor Ivory, circa 1967'),
          S('Logic is'),
        ]),
        L('a science.', [
          S('often defined as the science of'),
          L('logical truths.', [
            L('logical truths, which are a fundamental concept in logic.', [
              S('logical truths. A logical truth is'),
              L('not only true.', [
                S('true under all'),
                L('interpretations.', [
                  S('interpretations of its logical components.'),
                  B(),
                ]),
              ]),
            ]),
          ]),
        ]),
        S('Logic is')
      ]),
      L('a discipline', [
        S('the study of'),
        L('reasoning', [
          L('correct reasoning', [
            S('correct'),
            L('reasoning or', [
              L('reasoning.', [
                S('reasoning. Reasoning is a'),
                L('capacity.', [
                  L('conscious capacity.', [
                    S('capacity that allows conclusions to be drawn.')
                  ]),
                ]),
              ]),
              S('Logic is also the study of'),
            ]),
            S('good arguments'),
          ])
        ]),
      ])
    ]),
  ]
}
```


Here is the way I built this up. Start with a simple phrase.

```
script = [
  S('Logic is a discipline'),
]
```


This will render "Logic is a discipline" without any links. Now I want to add an expandable link to "a discipline", so I make this change:

```
script = [
  S('Logic is'),
  L('a discipline, [
    S('the study of reasoning'),
  ])
]
```


So, "Logic is" is rendered as static (S) text without a link. "a discipline" is
rendered as linked (L) text. When you click on "a discipline" it is replaced
with the static text "the study of reasoning."

I just iterated this way, recursively replacing pieces of static text with
linked text.

Apart from S (for static) and L (for linked) text there is also B (for line
Break) and P for picture. P takes two arguments, the name of the image and
caption text. quoted from above:

```
P('images/jivory.jpg', 'Professor Ivory, circa 1967')
```
