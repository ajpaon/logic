import ReactDOM from 'react-dom/client'
import React, {useState, useReducer, useMemo, useEffect, useCallback} from 'react'
import type { TextTree, TextItem } from '../lib/text'
import { ExpandableTextRenderer } from '../explorer/explorer'
import { openDB } from 'idb'

function TextNode({ content, replaceNode, displayedInRenderer }: { content: TextItem, replaceNode(node: TextItem): void, displayedInRenderer: boolean}) {
  const [editorState, setEditorState] = useState<'view' | 'edit'>('view')
  const [text, setText] = useState(content.text)

  function saveEdit() {
    setEditorState('view')
    replaceNode({tag: 'simple', text})
  }

  function cancelEdit() {
    setText(content.text)
    setEditorState('view')
  }

  function handleKeys(e: React.KeyboardEvent<HTMLInputElement>) {
    switch(e.key) {
      case 'Enter':
        saveEdit()
        return
      case 'Escape':
        cancelEdit()
        return
    }
  }

  switch(editorState) {
    case 'view':
      return <>{displayedInRenderer ? <b>{content.text}</b> : content.text } <button onClick={() => setEditorState('edit')}>Edit</button></>
    case 'edit':
      return <>
        <input
          type="text" value={text}
          onChange={(e) => setText(e.target.value)}
          onKeyDown={handleKeys}
        />
        <button onClick={saveEdit}>
          Update
        </button>
        <button onClick={cancelEdit}>
          Cancel
        </button>
      </>
  }
}

interface ExpandableTextEditorProps {
  tree: TextTree
  nodeId: string
  editor: EditorAPI
  depth: number
  expandedNodes: string[]
}

function NodeEditor({ node, replaceNode, displayedInRenderer}: {node: TextItem, replaceNode(node: TextItem): void, displayedInRenderer: boolean}) {
  switch(node.tag) {
    case 'simple':
      return <TextNode displayedInRenderer={displayedInRenderer} content={node} replaceNode={replaceNode} />
    default:
      return null
  }
}

function NodeControls({ nodeId, editor, expanded }: { nodeId: string, editor: EditorAPI, expanded: boolean }) {
  return <>
    <button onClick={() => editor.copyWithin(nodeId)}>
      Copy within
    </button>
    <button onClick={() => editor.copyBelow(nodeId)}>Copy below</button>
    <button onClick={() => editor.deleteNode(nodeId)}>Delete</button>
    {
      expanded && <button onClick={() => editor.collapseNode(nodeId)}>Rewind to here</button>
    }
  </>
}

function ExpandableTextEditor({ tree, editor, nodeId, depth, expandedNodes }: ExpandableTextEditorProps) {
  const node = tree.nodes[nodeId]
  const children = tree.children[nodeId]

  const bg = depth % 2 ? '#fff' : '#eee'
  const displayedInRenderer = !expandedNodes.includes(nodeId) && expandedNodes.includes(tree.parents[nodeId])

  return <div style={{padding: '5px', paddingRight: '0', margin: '5px', marginRight: '0', borderBottom: '1px solid black', borderLeft: '1px solid black', backgroundColor: bg}}>
    {
      node && <div>
        <NodeEditor node={node} displayedInRenderer={displayedInRenderer} replaceNode={(node: TextItem) => { editor.replaceNode(nodeId, node) }} />
        <NodeControls expanded={expandedNodes.includes(nodeId)} nodeId={nodeId} editor={editor} />
      </div>
    }
    {children && children.length !== 0 && children.map(childId => <ExpandableTextEditor key={childId} expandedNodes={expandedNodes} tree={tree} nodeId={childId} editor={editor} depth={depth+1} />) }
  </div>
}

interface ReplaceNodeAction {
  tag: 'replace-node'
  nodeId: string
  node: TextItem
}

interface InsertNodeAction {
  tag: 'copy-within'
  parentNodeId: string
  nodeId: string
}

interface CopyBelowAction {
  tag: 'copy-below'
  parentNodeId: string
  sourceId: string
  nodeId: string
}

interface DeleteNodeAction {
  tag: 'delete-node'
  parentNodeId: string
  nodeId: string
}

interface LoadDocument {
  tag: 'load-document'
  doc: Document
}

interface SetDocumentName {
  tag: 'set-document-name'
  name: string
}

type DocumentAction = ReplaceNodeAction | InsertNodeAction | CopyBelowAction | DeleteNodeAction | LoadDocument | SetDocumentName

function insertAfter(arr: string[], val: string, after: string) {
  const i = arr.indexOf(after)
  return [...arr.slice(0, i+1), val, ...arr.slice(i+1)]
}

function remove(arr: string[], val: string) {
  const i = arr.indexOf(val)
  return [...arr.slice(0, i), ...arr.slice(i+1)]
}

function documentReducer(doc: Document, action: DocumentAction): Document {
  switch(action.tag) {
    case 'replace-node':
      return {
        ...doc,
        modified: new Date(),
        tree: {
          ...doc.tree,
          nodes: {
            ...doc.tree.nodes,
            [action.nodeId]: action.node
          }
        }
      }
    case 'copy-within':
      return {
        ...doc,
        modified: new Date(),
        tree: {
          ...doc.tree,
          nodes: {...doc.tree.nodes, [action.nodeId]: doc.tree.nodes[action.parentNodeId] },
          children: {
            ...doc.tree.children,
            [action.parentNodeId]: [...doc.tree.children[action.parentNodeId], action.nodeId],
            [action.nodeId]: []
          },
          parents: {
            ...doc.tree.parents,
            [action.nodeId]: action.parentNodeId,
          }
        }
      }
    case 'copy-below':
      return {
        ...doc,
        modified: new Date(),
        tree: {
          ...doc.tree,
          nodes: {...doc.tree.nodes, [action.nodeId]: doc.tree.nodes[action.sourceId] },
          children: {
            ...doc.tree.children,
            [action.parentNodeId]: insertAfter(doc.tree.children[action.parentNodeId], action.nodeId, action.sourceId),
            [action.nodeId]: []
          },
          parents: {
            ...doc.tree.parents,
            [action.nodeId]: action.parentNodeId,
          }
        }
      }
    case 'delete-node':
      return {
        ...doc,
        modified: new Date(),
        tree: {
          ...doc.tree,
          nodes: Object.fromEntries(Object.keys(doc.tree.nodes).filter(nodeId => nodeId !== action.nodeId).map(nodeId => [nodeId, doc.tree.nodes[nodeId]])),
          children: {
            ...doc.tree.children,
            [action.parentNodeId]: remove(doc.tree.children[action.parentNodeId], action.nodeId),
            [action.nodeId]: []
          },
          parents: {
            ...doc.tree.parents,
            [action.nodeId]: 'root',
          }
        }
      }
    case 'load-document':
      return action.doc

    case 'set-document-name':
      return {
        ...doc,
        name: action.name,
        modified: new Date(),
      }
  }
}

interface EditorAPI {
  replaceNode(nodeId: string, node: TextItem): void
  copyWithin(parentNodeId: string): void
  copyBelow(nodeId: string): void
  deleteNode(nodeId: string): void
  collapseNode(nodeId: string): void
}

interface Document {
  name: string
  modified: Date
  tree: TextTree
}

interface DocumentAPI {
  saveDocument(doc: Document): Promise<void>
  loadDocument(name: string): Promise<Document>
  listDocuments(): Promise<Document[]>
}

interface DBSchema {
  documents: {
    value: Document
    key: string
    indexes: { 'dateIdx': Date }
  }
}

function useDocumentAPI() {
  const [db, setDb] = useState<DocumentAPI|null>(null)

  useEffect(() => {
    (async () => {
      const db = await openDB<DBSchema>('etext-db', 4, {
        upgrade(db, _oldVersion, _newVersion, tx) {
          const documentStore = db.objectStoreNames.contains('documents') ?
            tx.objectStore('documents') :
            db.createObjectStore('documents', { keyPath: 'name' })

          if (!documentStore.indexNames.contains('dateIdx')) {
            documentStore.createIndex('dateIdx', 'modified')
          }
        }
      })

      setDb({
        async loadDocument(name: string) {
          const doc = await db.get('documents', name) as Document
          return doc
        },

        async saveDocument(doc: Document) {
          await db.put('documents', doc)
        },

        listDocuments() {
          return db.getAllFromIndex('documents', 'dateIdx')
        },
      })
    })()
  }, [])

  return db
}

function DocumentSelector({documents, selectDocument}: {documents: Document[], selectDocument(doc: Document): void}) {
  return <div className='file-selector'>
    {
      documents.slice(0).reverse().map((doc: Document) => {
        return <div key={doc.name} onClick={() => selectDocument(doc)}>
          <span>{doc.modified.toString()}</span>
          <span style={{textDecoration: 'underline'}}>{doc.name}</span>
        </div>
      })
    }

  </div>
}

function useToggle(b: boolean): [boolean, () => void] {
  const [val, setVal] = useState(b)
  return [val, () => setVal(!val)]
}

function DebugPane({ tree }: { tree: TextTree }) {
  const json = useMemo(() => JSON.stringify(tree), [tree])
  const [collapsed, toggleCollapse] = useToggle(true)

  const button = useMemo(() => {
    return <button onClick={toggleCollapse}>{ collapsed ? 'Show' : 'Hide' }</button>
  }, [collapsed])

  return <div style={{marginTop: '20px', border: '1px solid black', padding: '5px'}}>

    <b>Debug info:</b>
    { button }
    <button onClick={() => {navigator.clipboard.writeText(json)}}>Copy to clipboard</button>
    {
      collapsed || <>
        <div style={{fontFamily: 'monospace', backgroundColor: '#ddd', margin: '5px'}}>
          {json}
        </div>
      </>
    }
  </div>
}

function DocumentPanel({ doc, loadDocument, setDocumentName: setDocumentTitle }: { doc: Document, loadDocument(doc: Document): void, setDocumentName(title: string): void }) {
  const db = useDocumentAPI()

  const [lastSaveTime, setLastSaveTime] = useState(doc.modified)
  const [fileSelectorOpen, setFileSelectorOpen] = useState(false)

  const [documents, setDocuments] = useState<Document[]>([])

  const refreshDocuments = useCallback(async () => {
    if (!db) return
    setDocuments(await db.listDocuments())
  }, [db])

  useEffect(() => {
    (async () => {
      await refreshDocuments()
    })()
  }, [refreshDocuments])

  function startNewDocument() {
    const doc = createNewDocument()
    setLastSaveTime(doc.modified)
    loadDocument(doc)
  }

  function chooseDifferentDocument(doc: Document) {
    setFileSelectorOpen(false)
    setLastSaveTime(doc.modified)
    loadDocument(doc)
  }

  function saveCurrentDocument() {
    // TODO: maybe throw an error here?
    if (!db) return
    db.saveDocument(doc)
    setLastSaveTime(doc.modified)
  }

  return <div style={{
    border: '1px solid black',
      margin: '5px',
      padding: '5px',
    }}>
    <label>Name: <input type="text" value={doc.name}
      onChange={e => setDocumentTitle(e.target.value)}
      onKeyDown={(e) => e.key === 'Enter' && saveCurrentDocument()}
    /></label>
    <button onClick={saveCurrentDocument} disabled={doc.modified === lastSaveTime}>Save changes</button>
    {
      fileSelectorOpen ?
        <button onClick={() => setFileSelectorOpen(false)}>Cancel</button> :
        <button onClick={() => setFileSelectorOpen(true)} disabled={doc.modified !== lastSaveTime}>Select a different Document</button>
    }
    <button onClick={refreshDocuments}>Refresh documents</button>
    <button onClick={startNewDocument} disabled={doc.modified !== lastSaveTime}>Create a new Document</button>
    {
      fileSelectorOpen && db && <DocumentSelector documents={documents} selectDocument={chooseDifferentDocument} />
    }
  </div>

}

function createNewDocument(): Document {
  const initialId = crypto.randomUUID()

  return {
    name: 'New Document',
    modified: new Date(),
    tree: {
      nodes: {
        [initialId]: { tag: 'simple', text: 'placeholder text' },
      },
      children: {
        'root': [initialId],
        [initialId]: []
      },
      parents: {
        [initialId]: 'root'
      },
    }
  }
}

function App() {
  const [doc, dispatch] = useReducer(documentReducer, createNewDocument())

  // TODO: eventually all the linear scans will slow things down
  const [expandedNodes, setExpandedNodes] = useState<string[]>(['root'])

  const editor: EditorAPI = useMemo(() => ({
    replaceNode(nodeId: string, node: TextItem) {
      dispatch({tag: 'replace-node', nodeId, node})
    },

    copyWithin(parentNodeId: string) {
      dispatch({tag: 'copy-within', parentNodeId, nodeId: crypto.randomUUID()})
    },

    copyBelow(nodeId: string) {
      const parentNodeId = doc.tree.parents[nodeId]
      dispatch({tag: 'copy-below', parentNodeId, sourceId: nodeId, nodeId: crypto.randomUUID() })
    },

    deleteNode(nodeId: string) {
      const parentNodeId = doc.tree.parents[nodeId]
      dispatch({tag: 'delete-node', nodeId, parentNodeId})
    },

    collapseNode(nodeId: string) {
      collapseNode(nodeId)
    }
  }), [doc, dispatch, expandedNodes])

  function expandNode(nodeId: string) {
    setExpandedNodes([nodeId, ...expandedNodes])
  }

  function descendants(nodeId: string) {
    const found = []
    const stack = [nodeId]
    while (stack.length) {
      const nodeId = stack.pop() as string
      found.push(nodeId)
      stack.push(...doc.tree.children[nodeId])
    }
    return found
  }

  function collapseNode(nodeId: string) {
    const toCollapse = descendants(nodeId)
    setExpandedNodes(expandedNodes.filter(i => !toCollapse.includes(i) ))
  }

  return <>
    <DocumentPanel doc={doc}
      loadDocument={doc => dispatch({tag: 'load-document', doc})}
      setDocumentName={name => dispatch({tag: 'set-document-name', name})}
    />

    <div style={{display: 'grid', gridTemplateColumns: '1fr 1fr'}}>
      <div style={{marginRight: '10px'}}>
        <ExpandableTextEditor tree={doc.tree} nodeId='root' editor={editor} depth={0} expandedNodes={expandedNodes} />
      </div>
      <div style={{border: '1px solid black', padding: '5px' }}>
        <ExpandableTextRenderer expandedNodes={expandedNodes} expandNode={expandNode} tree={doc.tree} nodeId='root' />
      </div>
    </div>

    <DebugPane tree={doc.tree} />
  </>
}

document.addEventListener('DOMContentLoaded', () => {
  const root = ReactDOM.createRoot(document.getElementById('react-root') as HTMLElement)
  root.render(<App />)
})
