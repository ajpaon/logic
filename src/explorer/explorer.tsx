import React, { useState } from 'react'
import type { TextTree, TextItem } from '../lib/text'

interface LinkedTextRendererProps {
  tree: TextTree
  nodeId: string
  expandedNodes: string[]
  expandNode(nodeId: string): void
}

function LinkedTextRenderer({ tree, nodeId, expandedNodes, expandNode }: LinkedTextRendererProps ) {
  const content = tree.nodes[nodeId]
  const childIds = tree.children[nodeId]
  const expanded = expandedNodes.includes(nodeId)
  function setExpanded() {
    expandNode(nodeId)
  }

  if (expanded) {
    return <>{childIds.map(childId => <ExpandableTextRenderer key={childId} tree={tree} nodeId={childId} expandedNodes={expandedNodes} expandNode={expandNode} />)}</>
  } else {
    return <span style={{color: 'blue', cursor: 'pointer', textDecoration: 'underline' }} onClick={setExpanded}>{ content.text }</span>
  }
}

function SimpleTextRenderer({ content }: { content: TextItem }) {
  return <span>{ content.text }</span>
}

interface ExpandableTextRendererProps {
  tree: TextTree
  nodeId: string
  expandedNodes: string[]
  expandNode(nodeId: string): void
}

export function ExpandableTextRenderer({ tree, nodeId, expandedNodes, expandNode }: ExpandableTextRendererProps) {
  if (nodeId === 'root') {
    return <>{tree.children['root'].map(childId => <ExpandableTextRenderer expandedNodes={expandedNodes} expandNode={expandNode} key={childId} tree={tree} nodeId={childId} />)}</>
  }

  if (tree.children[nodeId].length === 0) {
    return <><SimpleTextRenderer content={tree.nodes[nodeId]} />{' '}</>
  }

  return <><LinkedTextRenderer expandNode={expandNode} expandedNodes={expandedNodes} tree={tree} nodeId={nodeId}  />{' '}</>
}
