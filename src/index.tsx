import ReactDOM from 'react-dom/client'
import React from 'react'
import { S, L, P, B } from './lib/text'
import type { ExpandableText } from './lib/text'
import { ExpandableTextRenderer } from './explorer/explorer'

const EXAMPLE_CONTENTS: ExpandableText = {
  items: [
    S('Arguments are sets of'),
    L('statements.', [
      L('declarative statements.', [
        S('statements that'),
        L('have a truth value.', [
          S('can be true or false.'),
        ]),
      ]),
    ]),
    B(),

    S('Logic is the study of'),
    L('deductively valid arguments', [
      S('arguments that, if true, guarantee the truth of the arguments\' conclusions.',),
    ]),
    B(),

    L('Any given natural language', [
      S('English'),
    ]),
    S('is'),
    L('structured.', [
      L('grammatically structured.', [
        L('grammatically and logically structured.', [
          L('grammatically and logically structured.', [
            L('logically structured.', [
              S('logically structured, as evidenced by words like "and", "or", "not", "if...then", and "if and only if."')
            ])
          ]),
        ]),
      ])
    ])
  ]
}

function App() {
  return <ExpandableTextRenderer content={EXAMPLE_CONTENTS} />
}

document.addEventListener('DOMContentLoaded', () => {
  const root = ReactDOM.createRoot(document.getElementById('react-root') as HTMLElement)
  root.render(<App />)
})
