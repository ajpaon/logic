export interface LinkedText {
  tag: 'linked'
  text: string
  expansion: ExpandableText
}

export interface SimpleText {
  tag: 'simple'
  text: string
}

export interface ImageText {
  tag: 'image'
  href: string
  caption: string
}

export interface BreakText {
  tag: 'break'
}

export type TextItem = LinkedText | SimpleText

export interface ExpandableText {
  items: TextItem[]
}

export interface TextTree {
  nodes: Record<string, TextItem>
  children: Record<string, string[]>
  parents: Record<string, string>
}

export const S = (text: string): SimpleText => ({ tag: 'simple', text })
export const L = (text: string, content: TextItem[]): LinkedText => ({ tag: 'linked', text, expansion: { items:  content } })
export const P = (href: string, caption: string): ImageText => ({tag: 'image', href, caption})
export const B = (): BreakText => ({ tag: 'break' })

